﻿using API.Domain.PersonaProfile;
using API.Models;
using AutoMapper;

namespace API.Services
{
    public interface IPersonaService
    {
        Task<Persona> Created(PersonaRequest persona);

    }

    public class PersonaService : IPersonaService
    {
        private readonly IMapper _mapper;
        public PersonaService(IMapper mapper)
        {
            _mapper = mapper;
        }
        public async Task<Persona> Created(PersonaRequest persona)
        {
            var _persona = _mapper.Map<Persona>(persona);
            _persona.Id = Guid.NewGuid();
            _persona.Estado = "A";
            _persona.FechaIngreso = DateTime.UtcNow;
            _persona.UsuarioIngreso = "fmachuca";


            //logica
            //logica
            //logica
            //logica

            await Task.CompletedTask;
            return _persona;

        }
    }
}
