﻿using API.Models;
using AutoMapper;

namespace API.Domain.PersonaProfile
{
    public class PersonaProfileConfig : Profile
    {
        public PersonaProfileConfig()
        {

            CreateMap<Persona, PersonaDTO>()
                .ForMember(dest => dest.PersonaDireccionesDTO, opt => opt.MapFrom(src => src.PersonaDirecciones))
                .ReverseMap();

            CreateMap<PersonaRequest, Persona>()
                .ReverseMap();

            CreateMap<PersonaDireccionDTO, PersonaDireccion>()
                .ReverseMap();
        }
    }
}


