﻿namespace API.Domain.PersonaProfile
{
    public class PersonaDTO
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Altura { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public List<PersonaDireccionDTO> PersonaDireccionesDTO { get; set; }
    }

    public class PersonaDireccionDTO
    {
        public Guid Id { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
    }
}
