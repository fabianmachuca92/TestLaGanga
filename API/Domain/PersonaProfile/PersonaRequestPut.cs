﻿using System.ComponentModel.DataAnnotations;

namespace API.Domain.PersonaProfile
{
    public class PersonaRequestPut
    {
        public Guid Id { get; set; }

        [Required, StringLength(50, MinimumLength =3)]
        public string Nombre { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string Apellido { get; set; }

    }
}
