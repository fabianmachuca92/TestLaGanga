﻿using System.ComponentModel.DataAnnotations;

namespace API.Domain.PersonaProfile
{
    public class PersonaRequest
    {
        [Required, StringLength(50, MinimumLength =3)]
        public string Nombre { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string Apellido { get; set; }

        [Required]
        public DateTime FechaNacimiento { get; set; }
    }
}
