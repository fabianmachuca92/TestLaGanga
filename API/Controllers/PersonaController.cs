﻿using API.Domain.PersonaProfile;
using API.Models;
using API.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPersonaService _personaService;
        private readonly TestContext _context;

        public PersonaController(IMapper mapper, IPersonaService personaService, TestContext context)
        {
            _mapper = mapper;
            _personaService = personaService;
            _context = context;
        }

        // GET: api/<PersonaController>
        [HttpGet]
        public async Task<ActionResult<List<PersonaDTO>>> Get([FromQuery] string? nombre = null, [FromQuery] string? peso = null)
        {
            try
            {
                IQueryable<Persona> query = _context.Persona;
                
                if (nombre != null)
                    query = query.Where(a => a.Nombre.Contains(nombre));

                if (string.IsNullOrEmpty(peso))
                    query = query.Where(a => a.Peso == peso);


                var result = _mapper.Map<List<PersonaDTO>>(query.ToList());
                await Task.CompletedTask;
                return Ok(result);

            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        // GET api/<PersonaController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonaDTO>> GetId(Guid id)
        {

            var infoDB = new List<Persona> {
                new Persona("Fabian","Machuca"),
                new Persona("Pilar","PP"),
                new Persona("Santiago","Santana")
            };



            var result = infoDB.Where(a => a.Id == id).FirstOrDefault();

            if (result == null)
                return NotFound();
            await Task.CompletedTask;
            return Ok(result);
        }



        // POST api/<PersonaController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] PersonaRequest persona)
        {
            if (ModelState.IsValid)
            {
                if (DateTime.Now.Year - persona.FechaNacimiento.Year < 18)
                    return BadRequest("no es mayor de edad");

                var _persona = await _personaService.Created(persona);


                return CreatedAtAction(nameof(GetId), new { id = _persona.Id }, _persona);


            }
            return BadRequest(ModelState.ValidationState);
        }
















        //// POST api/<PersonaController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<PersonaController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<PersonaController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
