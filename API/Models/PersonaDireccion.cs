﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class PersonaDireccion: IEntity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid PersonaId { get; set; }

        public string Direccion { get; set; }
        public string Estado { get; set; }

        public virtual Persona? Persona { get; set; }

    }
}
