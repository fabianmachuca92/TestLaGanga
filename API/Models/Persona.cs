﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class Persona: IEntity
    {

        public Persona()
        {
        }

        public Persona(string nombre, string apellido)
        {
            Id = Guid.NewGuid();
            if (nombre == "Fabian")
                Id = Guid.Parse("ef3e64af-8174-4f61-b1a6-d9e4ea14e703");
            Nombre = nombre;
            Apellido = apellido;
        }

        [Key]
        public Guid Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }

        public string Altura { get; set; }
        public string Peso { get; set; }
        public string Estado { get; set; }
        public string UsuarioIngreso { get; set; }
        public DateTime FechaIngreso { get; set; }

        public virtual List<PersonaDireccion> PersonaDirecciones { get; set; }

    }
}
