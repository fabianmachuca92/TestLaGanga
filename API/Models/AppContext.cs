﻿using Microsoft.EntityFrameworkCore;

namespace API.Models
{
    public class TestContext : DbContext
    {
        public TestContext(DbContextOptions<TestContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Persona>

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Persona> Persona { get; set; }
        public DbSet<PersonaDireccion> PersonaDireccion { get; set; }
    }
}
